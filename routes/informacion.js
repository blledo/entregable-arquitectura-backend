var express = require('express');
var router = express.Router();

router.get('/', (req, res) => {
    let informacionConId = [];

    for(let i = 0; i < informacionArray.length; i++)
    {

      let informacion = informacionArray[i];

      if(informacion == null) {
        continue;
      }

      informacion.id = i;

      informacionConId.push(informacion);
    }

    console.log(informacionArray);

    res.json(informacionConId);
});

router.get('/:id', (req, res) => {
  if(req.params.id === undefined) {
    return res.sendStatus(400);
  }

  let informacionConId = informacionArray[req.params.id];

  informacionConId.id = informacionArray.indexOf(informacionArray[req.params.id]);

  console.log(informacionConId);

  res.json(informacionConId);
});

const informacionArray = [
    {
      nombres: 'Braulio',
      apellidos: 'Lledó',
      correo: 'brauliolledo@gmail.com',
      direccion: 'Dirección actualizada',
      telefono: 12312323
    }
  ];

router.post('/', (req,res) => {
    informacionArray.push(req.body.informacion);
    res.sendStatus(204);
});

router.put('/:id', (req, res) => {
  if(req.params.id === undefined || req.body.informacion === undefined) {
    return res.sendStatus(400);
  }

  informacionArray[req.params.id] = req.body.informacion;
  console.log(informacionArray);
  res.sendStatus(204);
});

router.delete('/:id', (req, res) => {
  if(req.params.id === undefined) {
    return res.sendStatus(400);
  }

  informacionArray[req.params.id] = null;

  console.log(informacionArray);
  res.sendStatus(204);
});

module.exports = router;
