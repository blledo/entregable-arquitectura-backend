const fs = require('fs');
var app = require('../app');


httpServer = require('http').createServer(app);
httpServer.listen(3000);

httpServer.on('listening', () => {
  console.log('Servidor HTTP iniciado en puerto 3000');
});