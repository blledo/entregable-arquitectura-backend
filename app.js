var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');

var informacionRouter = require('./routes/informacion.js');

var app = express();


var allowCrossDomain = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', ['http://localhost:4200']);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  if ('OPTIONS' == req.method) {
    res.send(200);
  } else {
    next();
  }
};

app.use(allowCrossDomain);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));

app.use('/informacion', informacionRouter);

module.exports = app;